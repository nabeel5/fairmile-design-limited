<?php
function mailAttachment($mailTo, $senderMail, $senderName, $mailSubject, $messageBody) 
{
	$uid = md5(uniqid(time()));
	$header = "From: ".$senderName." <".$senderMail.">\r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
	$msg = "This is a multi-part message in MIME format.\r\n";
	$msg .= "--".$uid."\r\n";
	$msg .= "Content-type:text/plain; charset=iso-8859-1\r\n";
	$msg .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
	$msg .= $messageBody."\r\n\r\n";
	mail($mailTo, $mailSubject, $msg, $header);
	header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

function mailSender($mailTo, $messageBody) 
{
	$senderMail = "no-reply@anymail.com";
	$senderName = "No Reply";
	$mailSubject = "Message";
	mailAttachment($mailTo, $senderMail, $senderName, $mailSubject, $messageBody);
}

function mailServer($request) 
{
		$mailTo = 'nabeelabbas36@yahoo.com';
		$messageBody = messageBody($request);
		mailSender($mailTo, $messageBody);
}

function messageBody($request) 
{
	$messageBody = $request['message'] . "\r\n\r\n" . "--\r\n" . 
	'Email: ' . $request['mailFrom'] . "\r\n" . 
	'Name: ' . $request['senderName'] . "\r\n" . 
	'Contact: ' . $request['senderPhoneNumber'];
	return $messageBody;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$requestData = [
		'subject' => !empty($_POST['subject']) ? $_POST['subject'] : "NA",
		'message' => !empty($_POST['textarea']) ? $_POST['textarea'] : "NA`",
		'mailFrom' => !empty($_POST['email']) ? $_POST['email'] : "NA",
		'senderName' => !empty($_POST['name']) ? $_POST['name'] : "NA",
		'senderPhoneNumber' => !empty($_POST['phone']) ? $_POST['phone'] : "NA"
	];
	mailServer($requestData);
}
?>